from django.contrib import admin
from .models import establecimiento
from .models import bodega
from .models import central_abastecimiento
from .models import usuario
from .models import perfil
from .models import producto
from .models import tipo_producto
from .models import stock
from .models import cambio_stock
from .models import historico

# Register your models here.
admin.site.register(central_abastecimiento)

class establecimientoAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['fkcentral', 'codigo_est']}),
        ('information', {'fields': ['nombre_est', 'fkbodega', 'ciudad_est', 'ubicacion_est']}),
    ]
admin.site.register(establecimiento, establecimientoAdmin)
class bodegaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['descripcion_bod', 'dimensiones_bod']}),
    ]
admin.site.register(bodega, bodegaAdmin)

class usuarioAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Datos de establecimiento', {'fields': ['fkestablecimiento', 'fkbodega', 'fkperfil']}),
        ('information personal', {'fields': ['username', 'email', 'password']}),
    ]
admin.site.register(usuario, usuarioAdmin)

admin.site.register(perfil)

class productoAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['nombre_producto']}),
        ('information', {'fields': ['fktipo_producto']}),
    ]
admin.site.register(producto, productoAdmin)
admin.site.register(tipo_producto)

class stockAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['fkproducto']}),
        ('information', {'fields': ['cantidad', 'estado_producto']}),
    ]
admin.site.register(stock, stockAdmin)
admin.site.register(cambio_stock)
admin.site.register(historico)
