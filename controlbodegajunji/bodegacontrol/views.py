from django.shortcuts import render
from .models import central_abastecimiento, establecimiento, bodega, usuario
from itertools import chain
# Create your views here.
def index(request):
    central_list = central_abastecimiento.objects.all()
    estab_list = establecimiento.objects.all()
    bodg_list = bodega.objects.all().order_by('descripcion_bod')
    usr_list = usuario.objects.all().order_by('fkestablecimiento')
    result_list = (chain(central_list, estab_list, bodg_list, usr_list)) #junta todos los querysets en una lista.
    return render(request, 'vistausuario/index.html', {'result_list': result_list,})
    #modificar la renderisacion, porque no entrega los datos de forma ordenada.
