from django.db import models

# Create your models here.
class central_abastecimiento(models.Model):
    """docstring for central_abastecimiento."""
    nombre_central= models.CharField(max_length=300)
    region_central= models.CharField(max_length=300)
    def __str__(self):
        return self.nombre_central
    # def __init__(self, arg):
    #     super(central_abastecimiento, self).__init__()
    #     self.arg = arg

class bodega(models.Model):
    """docstring for bodega."""
    descripcion_bod= models.CharField(max_length=300, null=True)
    dimensiones_bod= models.CharField(max_length=300, null=True)
    def __str__(self):
        return self.descripcion_bod
    # def __init__(self, arg):
    #     super(bodega, self).__init__()
    #     self.arg = arg

class establecimiento(models.Model):
    """docstring for establecimiento."""
    fkcentral= models.ForeignKey(central_abastecimiento, on_delete=models.CASCADE, null=True)
    fkbodega= models.ForeignKey(bodega, on_delete=models.CASCADE, null=True)
    nombre_est= models.CharField(max_length=300)
    codigo_est= models.IntegerField(null=True)
    ciudad_est= models.CharField(max_length=300)
    ubicacion_est= models.CharField(max_length=300)
    def __str__(self):
        return self.nombre_est
    # def __init__(self, arg):
    #     super(establecimiento, self).__init__()
    #     self.arg = arg

class perfil(models.Model):
    """docstring for perfil."""
    AUXILIAR= 'AUXILIAR'
    ENCARGADA= 'ENCARGADA'
    tipo_perfil= (
    (AUXILIAR, 'AUXILIAR'),
    (ENCARGADA, 'ENCARGADA'),
    )
    tipoprofile = models.CharField(
        max_length=10,
        choices= tipo_perfil,
        default= None,
    )
    def __str__(self):
        return self.tipoprofile
    # def __init__(self, arg):
    #     super(perfil, self).__init__()
    #     self.arg = arg

class tipo_producto(models.Model):
    """docstring for tipo_producto."""
    detalle_tipo_producto= models.CharField(max_length=300)
    categoria= (
        ('MOBILIARIO', 'MOBILIARIO'),
        ('DIDACTICO', 'DIDACTICO'),
        ('ELECTRONICO', 'ELECTRONICO'),
        ('ASEO', 'ASEO'),
    )
    categoria_producto= models.CharField(
        max_length=12,
        choices= categoria,
        default= None,
    )
    esFungible= models.BooleanField(default=True)
    fecha_caducidad= models.DateField(null=True)
    unidad_medida= models.CharField(max_length=200)
    def __str__(self):
        return self.categoria_producto
    # def __init__(self, arg):
    #     super(tipo_producto, self).__init__()
    #     self.arg = arg

class producto(models.Model):
    """docstring for producto."""
    fktipo_producto= models.ForeignKey(tipo_producto, on_delete=models.CASCADE, null=True)
    nombre_producto= models.CharField(max_length=45)
    def __str__(self):
        return self.nombre_producto
    # def __init__(self, arg):
    #     super(producto, self).__init__()
    #     self.arg = arg

class stock(models.Model):
    """docstring for stock."""
    fkproducto= models.ForeignKey(producto, on_delete=models.CASCADE, null=True)
    cantidad= models.IntegerField(null=False)
    estado= (
        ('CRITICO', 'CRITICO'),
        ('MEDIO', 'MEDIO'),
        ('NORMAL', 'NORMAL'),
    )
    estado_producto= models.CharField(
        max_length=8,
        choices= estado,
        default= None,
    )
    # def __init__(self, arg):
    #     super(stock, self).__init__()
    #     self.arg = arg


class usuario(models.Model):
    """docstring for usuario."""
    fkestablecimiento = models.ForeignKey(establecimiento, on_delete=models.CASCADE, null=True)
    fkbodega= models.ForeignKey(bodega, on_delete=models.CASCADE, null=True)
    fkperfil= models.ForeignKey(perfil, on_delete=models.CASCADE, null=True)
    username= models.CharField(max_length=300)
    email= models.EmailField(max_length=70,blank=True)
    password= models.CharField(max_length=25)
    def __str__(self):
        return self.username
    # def __init__(self, arg):
    #     super(usuario, self).__init__()
    #     self.arg = arg

class cambio_stock(models.Model):
    """docstring for cambio_stock."""
    fkstock= models.ForeignKey(stock, on_delete=models.CASCADE, null=True)
    fkusuario= models.ForeignKey(usuario, on_delete=models.CASCADE, null=True)
    fecha_cambio= models.DateField()
    # def __init__(self, arg):
    #     super(cambio_stock, self).__init__()
    #     self.arg = arg

class historico(models.Model):
    """docstring for historico."""
    fkcambiostock= models.ForeignKey(cambio_stock, on_delete=models.CASCADE, null=True)
    # def __init__(self, arg):
    #     super(historico, self).__init__()
    #     self.arg = arg
