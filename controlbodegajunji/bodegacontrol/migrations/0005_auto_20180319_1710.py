# Generated by Django 2.0.3 on 2018-03-19 20:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bodegacontrol', '0004_bodega_fkestablecimiento'),
    ]

    operations = [
        migrations.CreateModel(
            name='central_abastecimiento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_central', models.CharField(max_length=300)),
                ('region_central', models.CharField(max_length=300)),
            ],
        ),
        migrations.AddField(
            model_name='establecimiento',
            name='fkcentral',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='bodegacontrol.central_abastecimiento'),
        ),
    ]
