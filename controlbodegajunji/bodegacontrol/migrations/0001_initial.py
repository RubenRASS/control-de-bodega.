# Generated by Django 2.0.3 on 2018-03-13 14:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='bodega',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=200)),
                ('dimensiones', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='centralabastecimiento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('region', models.CharField(max_length=200)),
                ('nombre_central', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='change_stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_cambio', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='establecimiento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_establecimiento', models.CharField(max_length=200)),
                ('ciudad_establecimiento', models.CharField(max_length=200)),
                ('codigo_jardin', models.IntegerField()),
                ('direccion', models.CharField(max_length=200)),
                ('estado_jardin', models.BooleanField(verbose_name=True)),
            ],
        ),
        migrations.CreateModel(
            name='product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_producto', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='stock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado_stock_prod', models.CharField(max_length=200)),
                ('cantidad', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='type_product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_caducidad', models.DateField()),
                ('detalle', models.CharField(max_length=200)),
                ('categoria', models.CharField(max_length=200)),
                ('esFungible', models.BooleanField(verbose_name=False)),
                ('unidad_medida', models.CharField(max_length=200)),
                ('codigo_junji', models.IntegerField(null=True)),
                ('n_guia', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=200)),
                ('email', models.EmailField(max_length=75)),
                ('password', models.CharField(max_length=200)),
            ],
        ),
    ]
