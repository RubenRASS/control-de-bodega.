# Generated by Django 2.0.3 on 2018-03-20 13:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bodegacontrol', '0007_auto_20180319_1740'),
    ]

    operations = [
        migrations.CreateModel(
            name='producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_producto', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='tipo_producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('detalle_tipo_producto', models.CharField(max_length=300)),
                ('categoria_producto', models.CharField(choices=[('MOB', 'MOBILIARIO'), ('DID', 'DIDACTICO'), ('ELE', 'ELECTRONICO'), ('ASE', 'ASEO')], default=None, max_length=3)),
                ('esFungible', models.BooleanField(default=True)),
                ('fecha_caducidad', models.DateField(null=True)),
                ('unidad_medida', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='perfil',
            name='tipoprofile',
            field=models.CharField(choices=[('AU', 'AUXILIAR'), ('EN', 'ENCARGADA')], default=None, max_length=2),
        ),
        migrations.AddField(
            model_name='producto',
            name='fktipo_producto',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='bodegacontrol.tipo_producto'),
        ),
    ]
