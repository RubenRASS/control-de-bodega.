# Generated by Django 2.0.3 on 2018-03-19 18:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bodegacontrol', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='centralabastecimiento',
        ),
        migrations.DeleteModel(
            name='change_stock',
        ),
        migrations.DeleteModel(
            name='establecimiento',
        ),
        migrations.DeleteModel(
            name='product',
        ),
        migrations.DeleteModel(
            name='stock',
        ),
        migrations.DeleteModel(
            name='type_product',
        ),
        migrations.DeleteModel(
            name='usuario',
        ),
        migrations.RemoveField(
            model_name='bodega',
            name='descripcion',
        ),
        migrations.RemoveField(
            model_name='bodega',
            name='dimensiones',
        ),
        migrations.AddField(
            model_name='bodega',
            name='descripcion_bod',
            field=models.CharField(max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='bodega',
            name='dimensiones_bod',
            field=models.CharField(max_length=300, null=True),
        ),
        migrations.AddField(
            model_name='bodega',
            name='numero_bodega',
            field=models.IntegerField(null=True),
        ),
    ]
